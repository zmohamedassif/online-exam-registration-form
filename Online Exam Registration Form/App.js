import * as React from 'react';
import { Text, View, StyleSheet,Picker,ScrollView,Alert } from 'react-native';

// You can import from local files

import { Appbar } from 'react-native-paper';

// or any pure javascript modules available in npm
import { Headline } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import { Subheading } from 'react-native-paper';
import { Button } from 'react-native-paper';
import { Checkbox } from 'react-native-paper';

export default class App extends React.Component {
   state = {
    text: '',
    name: '',
    date: '',
    month: '',
    year: '',
    email: '',
    password: '',
    address: '',
    phone: '',
    checked: 'male',
    checked1: false,
    city:'Trichy',
  };
   login = (text1,text2,radio,text3,text4,text5,text6,text7,city,text8,declaration) => {
  alert('Registration Successful:'+'\n'+'Details:'+'\n'+'Name : '+ text1 + '\n' +
  'Date of Birth: '+   text2 +' '+ text3 +' '+text4+'\n'+
  'Gender: '+radio + '\n' + 
   'Email :'+text5 + '\n' +  
   'Password :'+text6  + '\n' + 
   'Address :'+text7 + '\n' + 
   'City :'+city + '\n' + 
   'Phone :'+text8 + '\n' + 
   'Read Terms of Condition :'+declaration )
  }
  render() {
    const { checked } = this.state;
    const { checked1 } = this.state;
   
    return (
      <View> 
        <Appbar.Header>
            <Appbar.Content 
              title="Online Exam Registration Form"
            />
        </Appbar.Header>
      <ScrollView contentContainerStyle={{ height: 1200 }}>
        <View>
         

          <Headline style={styles.heading}>Personal Information</Headline>

          <View  style={styles.details}>

            <TextInput
            label='Name'
            value={this.state.name}
            onChangeText={name => this.setState({ name })}
            />

            <Subheading>Date of Birth:</Subheading>
            <View  style={styles.flex}>
              <TextInput
              label='Date'
              style={{flex: 1}}
              keyboardType='numeric'
              maxLength={2}               
              value={this.state.date}
              onChangeText={date => this.setState({ date })}
              />
              <TextInput
              label='month'
              style={{flex: 1}}
              keyboardType='numeric'
              maxLength={2} 
              value={this.state.month}
              onChangeText={month => this.setState({ month })}
              />
              <TextInput
              label='Year'
              style={{flex: 1}}
              keyboardType='numeric'
              value={this.state.year}
              maxLength={4} 
              onChangeText={year => this.setState({ year })}
              />
            </View>

            <Subheading>Gender:</Subheading>
            <View style={styles.flex}>
              <RadioButton
                  value="male"
                  status={checked === 'male' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'male' }); }}
                />
              <Subheading>Male</Subheading>             
            </View>
            <View style={styles.flex}>
              <RadioButton
                value="female"
                status={checked === 'female' ? 'checked' : 'unchecked'}
                onPress={() => { this.setState({ checked: 'female' }); }}
              />
              <Subheading>Female</Subheading>              
            </View>

        </View>

          <Headline style={styles.heading}>Exam Account Information</Headline>

          <View  style={styles.details}>
            <TextInput
            label='Email'
            value={this.state.email}
            style={styles.text}
            onChangeText={email => this.setState({ email })}
            />

            <TextInput
            label='Password'
            value={this.state.password}
            style={styles.text}
            secureTextEntry= {true}
            onChangeText={password => this.setState({ password })}
            />
          </View> 

          <Headline style={styles.heading}>Contact Information</Headline>

          <View  style={styles.details}>  
            <TextInput
            label='Address'
            value={this.state.address}
            onChangeText={address => this.setState({ address })}
            /> 

            <Subheading>City:</Subheading>
            <Picker
              selectedValue={this.state.city}
              style={{height: 50, width: 150}}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({city: itemValue})
              }>
              <Picker.Item label="Trichy" value="Trichy" />
              <Picker.Item label="Chennai" value="Chennai" />
              <Picker.Item label="Madurai" value="Madurai" />
              <Picker.Item label="Coimabatore" value="Coiambatore" />
            </Picker>    

            <TextInput
            label='Phone'
            keyboardType='numeric'
            value={this.state.phone}
            style={styles.text}
            onChangeText={phone => this.setState({ phone })}
            maxLength={10} 
            va
            />  
          </View>

            <View style={styles.flex}>    
             <Checkbox
              status={checked1 ? 'checked' : 'unchecked'}
              onPress={() => { this.setState({ checked1: !checked1 }); }}
            />
            <Subheading>I have read and agreed to the terms and       conditions</Subheading>     
            </View>

           <Button mode="contained" onPress = {
          () => {fetch('http://172.17.4.181:3000/register1', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
               Name: this.state.name,DateOfBirth:[this.state.date ,this.state.month,                  this.state.year],Gender: this.state.checked,Email:                                    this.state.email ,Password:this.state.password
            ,Address:this.state.address,City:this.state.city,Phone:this.state.phone,                Agreed:this.state.checked1
              }),
            })
            .then(res => this.login(this.state.name,                    this.state.date ,this.state.checked,this.state.month,this.state.year,                                     this.state.email ,this.state.password
            ,this.state.address,this.state.city,this.state.phone,this.state.checked1))
            .catch(err => console.warn(err));
          }
        }>
          Register
        </Button>
            
        </View>
      
      </ScrollView>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  flex:{
    flexDirection:'row',
  },
  details:{
    marginRight:5,
    marginLeft:5,
  },
  text:{
   marginBottom:5,
  },
  heading:{
   fontSize:20
  },
});